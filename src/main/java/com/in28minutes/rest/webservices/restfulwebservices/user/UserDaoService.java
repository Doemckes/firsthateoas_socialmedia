package com.in28minutes.rest.webservices.restfulwebservices.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class UserDaoService {
	
	private static List<User> users = new ArrayList<>();


	private static int usersCount = 3;

	static {
		users.add(new User(1, "Adam", new Date()));
		users.add(new User(2, "Eve", new Date()));
		User jack = new User(3, "Jack", new Date());
		/*jack.addPost(new Post(1, "I am here", new Date()));
		jack.addPost(new Post(2, "second post", new Date()));*/
		users.add(jack);
	}

	public List<User> findAll() {
		return users;
	}

	public User save(User user) {
		if (user.getId() == null) {
			user.setId(++usersCount);
		}
		users.add(user);
		return user;
	}

	public User findOne(int id) {
		for (User user : users) {
			if (user.getId() == id) {
				return user;
			}
		}
		return null;
	}

	public User deleteById(int id) {
		Iterator<User> iterator = users.iterator();
		while (iterator.hasNext()) {
			User user = iterator.next();
			if (user.getId() == id) {
				iterator.remove();
				return user;
			}
		}
		return null;
	}

	/*public Post deleteUsersPostbyIDs(int uid,int pid) {
		Iterator<User> iterator = users.iterator();
		while (iterator.hasNext()) {
			User user = iterator.next();
			if (user.getId() == uid) {
				Iterator<Post> postiterator = user.getPosts().iterator();
				while (postiterator.hasNext()) {
					Post post = postiterator.next();
					if (post.getId() == pid) {
						postiterator.remove();
						return post;
					}
				}
			}
		}
		return null;
	}*/

/*	public User addPostToUser(int id, String text) {
		Iterator<User> iterator = users.iterator();
		while (iterator.hasNext()) {
			User user = iterator.next();
			if (user.getId() == id) {
				Post post = new Post(user.getPosts().size()+1,text,new Date());
				user.addPost(post);
				return user;
			}
		}
		return null;
	}*/

/*public Post addPostToUser(int id, Post post) {
	Iterator<User> iterator = users.iterator();
	while (iterator.hasNext()) {
		User user = iterator.next();
		if (user.getId() == id) {
			if (post.getId() == null) {
				post.setId(user.getPosts().size()+1);
			}
			user.addPost(post);
			return post;
		}
	}
	return null;
}*/
/*	public List<Post> getAllPostsOfUser(int id) {
		Iterator<User> iterator = users.iterator();
		while (iterator.hasNext()) {
			User user = iterator.next();
			if (user.getId() == id) {
				return user.getPosts();
			}
		}
		return null;
	}*/


}
