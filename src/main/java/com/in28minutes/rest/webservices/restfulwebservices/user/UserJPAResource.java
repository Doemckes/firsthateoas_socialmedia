package com.in28minutes.rest.webservices.restfulwebservices.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class UserJPAResource {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PostRepository postRepository;

	@GetMapping("/jpa/users")
	public List<User> retrieveAllUsers() {
		return userRepository.findAll();
	}

	@GetMapping("/jpa/users/{id}")
	public Resource<User> retrieveUser(@PathVariable int id) {
		Optional<User> user = userRepository.findById(id);

		if(!user.isPresent())
			throw new NotFoundException("id-"+ id);

		//HATEOAS!!!
		Resource<User> resource = new Resource<User>(user.get());
		ControllerLinkBuilder linktoAll = linkTo(methodOn(this.getClass()).retrieveAllUsers());
		resource.add(linktoAll.withRel("all-users"));
		ControllerLinkBuilder linktoPosts = linkTo(methodOn(this.getClass()).retrieveUserAllPosts(id));
		resource.add(linktoPosts.withRel("all-users-posts"));
		return resource;


	}

	@GetMapping("/jpa/users/{id}/posts/{pid}")
	public Post retrievePostOfUser(@PathVariable int id, @PathVariable int pid) {
		Optional<User> user = userRepository.findById(id);

		if(!user.isPresent())
			throw new NotFoundException("id-"+ id);

		for(Post post : user.get().getPosts()){
			if (post.getId() == pid){
				return post;
			}
		}
		throw new NotFoundException("id-"+ pid);
	}

	@GetMapping("/jpa/users/{id}/posts")
	public List<Post> retrieveUserAllPosts(@PathVariable int id) {
		Optional<User> user = userRepository.findById(id);

		if(!user.isPresent())
			throw new NotFoundException("id-"+ id);


		return user.get().getPosts();
	}

	@DeleteMapping("/jpa/users/{id}")
	public void deleteUser(@PathVariable int id) {
		userRepository.deleteById(id);
	}

    @DeleteMapping("/jpa/users/{uid}/posts/{pid}")
    public void deletePostOfUser(@PathVariable int uid,@PathVariable int pid) {
		Optional<User> optionaluser = userRepository.findById(uid);
		if(!optionaluser.isPresent())
			throw new NotFoundException("uid-"+ uid);

		Optional<Post> optionalpost = postRepository.findById(pid);
		if(!optionalpost.isPresent())
			throw new NotFoundException("pid-"+ pid);
		Integer postuserId = optionalpost.get().getUser().getId();
		Integer userId = optionaluser.get().getId();
		if (!postuserId.equals(userId)){
			throw new IllegalArgumentException("uid und UserId aus Post passen nicht zusammen: "+ userId + ", " + postuserId);
		}
		postRepository.deleteById(pid);


    }
	//
	// input - details of user
	// output - CREATED & Return the created URI
	@PostMapping("/jpa/users")
	public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
		User savedUser = userRepository.save(user);
		// CREATED
		// /user/{id}     savedUser.getId()

		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(savedUser.getId()).toUri();

		return ResponseEntity.created(location).build();

	}

	//
	// input - details of a Post
	// output - CREATED & Return the created URI
	@PostMapping("/jpa/users/{id}/posts")
	public ResponseEntity<Object> createPostForUser(@PathVariable int id,@RequestBody Post post) {
		Optional<User> optionaluser = userRepository.findById(id);
		if(!optionaluser.isPresent())
			throw new NotFoundException("id-"+ id);

		User user = optionaluser.get();
		post.setUser(user);
		postRepository.save(post);

		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(post.getId()).toUri();

		return ResponseEntity.created(location).build();

	}
}
