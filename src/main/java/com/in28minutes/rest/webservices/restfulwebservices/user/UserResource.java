package com.in28minutes.rest.webservices.restfulwebservices.user;

import java.net.URI;
import java.util.List;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;
import javax.validation.Valid;

@RestController
public class UserResource {

	@Autowired
	private UserDaoService service;

	@GetMapping("/users")
	public List<User> retrieveAllUsers() {
		return service.findAll();
	}

	@GetMapping("/users/{id}")
	public Resource<User> retrieveUser(@PathVariable int id) {
		User user = service.findOne(id);

		if(user==null)
			throw new NotFoundException("id-"+ id);

		//HATEOAS!!!
		Resource<User> resource = new Resource<User>(user);
		ControllerLinkBuilder linkto = linkTo(methodOn(this.getClass()).retrieveAllUsers());
		resource.add(linkto.withRel("all-users"));
		return resource;
	}

	/*@GetMapping("/users/{id}/posts/{pid}")
	public Post retrievePostOfUser(@PathVariable int id, @PathVariable int pid) {
		User user = service.findOne(id);

		if(user==null)
			throw new NotFoundException("id-"+ id);

		for(Post post : user.getPosts()){
			if (post.getId() == pid){
				return post;
			}
		}
		throw new NotFoundException("id-"+ pid);
	}

	@GetMapping("/users/{id}/posts")
	public List<Post> retrieveUserAllPosts(@PathVariable int id) {
		User user = service.findOne(id);

		if(user==null)
			throw new NotFoundException("id-"+ id);

		return user.getPosts();
	}*/

	@DeleteMapping("/users/{id}")
	public void deleteUser(@PathVariable int id) {
		User user = service.deleteById(id);
		
		if(user==null)
			throw new NotFoundException("id-"+ id);
	}

    /*@DeleteMapping("/users/{uid}/posts/{pid}")
    public void deletePostOfUser(@PathVariable int uid,@PathVariable int pid) {
        Post post = service.deleteUsersPostbyIDs(uid,pid);

        if(post==null)
            throw new NotFoundException("uid-"+ uid + ", + pid-" +pid);
    }
*/
	//
	// input - details of user
	// output - CREATED & Return the created URI
	@PostMapping("/users")
	public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
		User savedUser = service.save(user);
		// CREATED
		// /user/{id}     savedUser.getId()

		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(savedUser.getId()).toUri();

		return ResponseEntity.created(location).build();

	}

	//
	// input - details of a Post
	// output - CREATED & Return the created URI
	/*@PostMapping("/users/{id}/posts")
	public ResponseEntity<Object> createPostForUser(@PathVariable int id,@RequestBody Post post) {
		Post savedPost = service.addPostToUser(id, post);
		// CREATED
		// /user/{id}/posts     savedPost.getId()

		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(savedPost.getId()).toUri();

		return ResponseEntity.created(location).build();

	}*/
}
