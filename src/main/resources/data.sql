insert into user values (10001, sysdate(), 'Daniel');
insert into user values (10002, sysdate(), 'Tom');
insert into user values (10003, sysdate(), 'Daniela');
insert into user values (10004, sysdate(), 'Dennis');
insert into user values (10005, sysdate(), 'Blake');

insert into post values (11001, 'First Post', 10001);
insert into post values (11002, 'Second Post', 10001);